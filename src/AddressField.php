<?php

namespace Thunger\SilverstripeAddressAutocomplete;

use SilverStripe\Forms\TextField;
use SilverStripe\i18n\i18n;
use SilverStripe\Security\Security;
use SilverStripe\View\Requirements;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Manifest\ModuleResourceLoader;
use TractorCow\Fluent\State\FluentState;

class AddressField extends TextField {

	private static $default_options = [
		'enableCountryRestriction' => true,
		'setLatitudeLongitude' => false,
		'fieldMappings' => array(
			'Address' => [
				[
					'name' => 'street_number',
					'component' => 'short_name'
				],
				[
					'name' => 'route',
					'component' => 'long_name'
				]
			],
			'AddressLine2' => [
				[
					'name' => 'sublocality_level_1',
					'component' => 'long_name'
				],
				[
					'name' => 'neighborhood',
					'component' => 'long_name'
				]
			],
			'PostalCode' => [
				[
					'name' => 'postal_code',
					'component' => 'long_name'
				]
			],
			'City' => [
				[
					'name' => 'locality',
					'component' => 'long_name'
				],
				[
					'name' => 'postal_town',
					'component' => 'long_name'
				]
			],
			'State' => [
				[
					'name' => 'administrative_area_level_1',
					'component' => 'long_name'
				]
			],
			'Country' => [
				[
					'name' => 'country',
					'component' => 'short_name'
				]
			]
		)
	];

	protected $options = [];

	public function __construct($name, $title = null, $value = '', $maxLength = null, $form = null) {
		parent::__construct($name, $title, $value, $maxLength, $form);
		$this->setOptions();
	}

	public function setOptions(array $options = []) {
		$this->options = $this->config()->default_options;
		foreach ($this->options as $name => &$value) {
			if (isset($options[$name])) {
				if (is_array($value)) {
					$value = array_merge($value, $options[$name]);
				} else {
					$value = $options[$name];
				}
			}
		}
	}

	public function Field($properties = []) {
		if (class_exists(FluentState::class)) {
			$locale = FluentState::singleton()->getLocale();
		} else {
			$locale = i18n::get_locale();
		}
		$localeparts = explode('_', $locale);
		$Country = end($localeparts);
		if ($user = Security::getCurrentUser()) {
			$Country = $user->Country ?: $Country;
		}

		// field name
		$name = '';
		$fieldlist = $this->getContainerFieldList();
		if ($fieldlist && $containerfield = $fieldlist->getContainerField()) {
			$name = $containerfield->Name;
		}
		if (!$name) {
			$name = $this->getForm()->getName();
		}
		$this->setAttribute('data-address-autocomplete', $name);
		$this->setAttribute('data-country', $Country);
		$this->setAttribute('data-settings', json_encode($this->options));
		$this->setAttribute('autocomplete', 'off');
		$this->setAttribute('placeholder', 'Start typing your address');
		$this->requireDependencies();
		return parent::Field($properties);
	}

	protected function requireDependencies() {
		$MapsApiRequirements = array_filter(
			Requirements::backend()->getJavascript(),
			function ($key) {
				return
					strpos($key, 'maps.google.com/maps/api') !== false
					|| strpos($key, 'maps.googleapis.com/maps/api') !== false;
			},
			ARRAY_FILTER_USE_KEY
		);
		if (!count($MapsApiRequirements)) {
			Requirements::javascript(
				sprintf(
					'//maps.googleapis.com/maps/api/js?key=%s&libraries=places',
					Config::inst()->get('GoogleGeocoding', 'browser_key')
				),
				['defer' => true]
			);
		}
		Requirements::javascript(
			ModuleResourceLoader::singleton()->resolvePath(
				'thunger/silverstripe-addressautocomplete:javascript/addressfield.js'
			),
			['defer' => true]
		);
	}

	public function getOption($name) {
		// Quicker execution path for "."-free names
		if (strpos($name, '.') === false) {
			if (isset($this->options[$name])) {
				return $this->options[$name];
			}
		} else {
			$names = explode('.', $name);

			$var = $this->options;

			foreach ($names as $n) {
				if (!isset($var[$n])) {
					return null;
				}
				$var = $var[$n];
			}

			return $var;
		}
	}

	public function Type() {
		return 'text';
	}

}