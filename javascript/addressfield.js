jQuery(document).ready(function ($) {
	(function ($) {
		$.fn.enableAddressLookup = function () {
			$(this).each(function () {
				var
					autocomplete,
					$this = $(this),
					containerName = $this.attr('data-address-autocomplete'),
					$parent = $this.closest('.form-group'),
					parentIDparts = $parent.attr('id').split('_' + $this.attr('name') + '_'),
					parentSelector = parentIDparts.length ? '[id^=' + parentIDparts[0] + ']' : 'document',
					settings = JSON.parse($this.attr('data-settings')),
					autocompleteoptions = {
						types: ['address']
					},
					country = $this.attr('data-country');

				// prevent form submission on enter
				$(this).keydown(function (e) {
					if (e.which === 13) {
						return false;
					}
				});

				// country
				if (country && settings.enableCountryRestriction) {
					autocompleteoptions['componentRestrictions'] = {country: country.toLowerCase()}
				}

				// create autocompleter
				autocomplete = new google.maps.places.Autocomplete(this, autocompleteoptions);
				google.maps.event.addListener(autocomplete, 'place_changed', function () {
					var place = autocomplete.getPlace();
					updateAddressFields(place, containerName);
					if(settings.setLatitudeLongitude) {
						$(':input[name$=' + containerName + '_Latitude]').val(place.geometry.location.lat());
						$(':input[name$=' + containerName + '_Longitude]').val(place.geometry.location.lng());
						$('[id*=' + containerName + '] :input[name$=_Latitude]').val(place.geometry.location.lat());
						$('[id*=' + containerName + '] :input[name$=_Longitude]').val(place.geometry.location.lng());
					}
				});

				function updateAddressFields(place) {
					for (var key in settings.fieldMappings) {
						var val = '';
						if (settings.fieldMappings.hasOwnProperty(key)) {
							for (var i = 0; i < settings.fieldMappings[key].length; i++) {
								var tmpval = getPlaceComponent(place, settings.fieldMappings[key][i]);
								if (val) {
									val += ' ';
								}
								if (typeof tmpval != 'undefined') {
									val += tmpval;
								}
							}
						}
						var $input = $('[id*=' + containerName + '] :input[name$=_' + key + ']');
						if (!$input.length) {
							$input = $('[id*=' + containerName + '] :input[name=' + key + ']');
						}
						if ($input.length) {
							$input.val(val).trigger('change');
							if ($input.hasClass('has-chzn')) {
								$input.trigger('liszt:updated');
							}
						}
					}
				}

				function getPlaceComponent(place, item) {
					for (var i = 0; i < place.address_components.length; i++) {
						var addressType = place.address_components[i].types[0];
						if (item.name == addressType) {
							return place.address_components[i][item.component];
						}
					}
				}
			});
		};
	})(jQuery);

	if (typeof $.entwine === 'function') {
		$('input[data-address-autocomplete]').entwine({
			onmatch: function () {
				this.enableAddressLookup();
			}
		});
	} else {
		$('input[data-address-autocomplete]').enableAddressLookup();
	}
});
